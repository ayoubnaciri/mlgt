"""Analyze a portfolio."""

import pandas as pd
import numpy as np
import datetime as dt
from util import get_data, plot_data

#---------------------------------------------
#   compute pf stats                      
#---------------------------------------------
def pf_stats( pfval_hist, daily_rfr, sf ):
    cumulative_return = (pfval_hist[-1]/pfval_hist[0]) -1
    daily_return = pfval_hist.pct_change()
    avg_daily_return = daily_return.mean()
    daily_return_stdv = daily_return.std()
    
    risk_adj_daily_return = daily_return - daily_rfr
    sharpe_ratio = np.sqrt(sf)* (daily_return - daily_rfr).mean()/daily_return_stdv
    return cumulative_return, avg_daily_return, daily_return_stdv, sharpe_ratio 


# This is the function that will be tested by the autograder
# The student must update this code to properly implement the functionality
def assess_portfolio(sd = dt.datetime(2008,1,1), ed = dt.datetime(2009,1,1), \
    syms = ['GOOG','AAPL','GLD','XOM'], \
    allocs=[0.1,0.2,0.3,0.4], \
    sv=1000000, rfr=0.0, sf=252.0, \
    gen_plot=False):

    # Read in adjusted closing prices for given symbols, date range
    dates = pd.date_range(sd, ed)
    prices_all = get_data(syms, dates)  # automatically adds SPY
    prices = prices_all[syms]  # only portfolio symbols
    # order is important to avoid peaking into the future ffil before bfil
    prices.fillna(method="ffill", inplace=True)
    prices.fillna(method="bfill", inplace=True)
    
    prices_SPY = prices_all['SPY']  # only SPY, for comparison later

    # Get daily portfolio value. SPY as benchmark comparison with pf
    port_val = prices_SPY

    # add code here to compute daily portfolio values
    # -----------------------------------------------------
    normed_prices = prices/prices.ix[0,:]

    # dollar val allocations
    posval_hist = normed_prices * allocs * sv

    # daily pf value for each symbol in pf
    pfval_hist = posval_hist.sum(axis=1)

    # portfolio stats
    cr, adr, sddr, sr = pf_stats( pfval_hist, rfr, sf )
    
    # -----------------------------------------------------

    # Get portfolio statistics (note: std_daily_ret = volatility)
    # cr, adr, sddr, sr = [0.25, 0.001, 0.0005, 2.1] # add code here to compute stats

    # Compare daily portfolio value with SPY using a normalized plot
    if gen_plot:
        # add code to plot here
        df_temp = pd.concat([port_val, prices_SPY], keys=['Portfolio', 'SPY'], axis=1)
        pass

    # Add code here to properly compute end value
    ev = pfval_hist[-1]

    return cr, adr, sddr, sr, ev

def test_code():
    # This code WILL NOT be tested by the auto grader
    # It is only here to help you set up and test your code

    # Define input parameters
    # Note that ALL of these values will be set to different values by
    # the autograder!
    start_date = dt.datetime(2009,1,1)
    end_date = dt.datetime(2010,1,1)
    symbols = ['GOOG', 'AAPL', 'GLD', 'XOM']
    allocations = [0.2, 0.3, 0.4, 0.1]
    start_val = 1000000  
    risk_free_rate = 0.0
    sample_freq = 252

    # Assess the portfolio
    cr, adr, sddr, sr, ev = assess_portfolio(sd = start_date, ed = end_date,\
        syms = symbols, \
        allocs = allocations,\
        sv = start_val, \
        gen_plot = False)

    # Print statistics
    print "Start Date:", start_date
    print "End Date:", end_date
    print "Symbols:", symbols
    print "Allocations:", allocations
    print "Sharpe Ratio:", sr
    print "Volatility (stdev of daily returns):", sddr
    print "Average Daily Return:", adr
    print "Cumulative Return:", cr

if __name__ == "__main__":
    test_code()
